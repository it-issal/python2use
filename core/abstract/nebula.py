#-*- coding: utf-8 -*-

from python2use.core.meta import *

class Backend(Manageable):
    EP_MASK = '%(provider)s://%(owner)s@%(host)s:%(port)d/%(name)s'
    
    TPL_NARROW  = '%(owner)s=%(name)s'
    BARE_PATH   = ['/shl', 'var', 'lib', 'nebula']
    
    CONTEXT_CORE  = ('provider', 'ports')
    CONTEXT_EXTRA = ('commandline', 'ctl_path', 'cfg_path', 'host')
    
    def __init__(self, mgr, owner, name, **kwargs):
        self._prvd  = kwargs['provider'] ; del kwargs['provider']
        
        super(Backend, self).__init__(mgr, owner, name, **kwargs)
    
    provider = property(lambda self: self._prvd)
    host     = property(lambda self: Nucleon.local.hostname)
    
    job_path = property(lambda self: os.path.join(*(self.BARE_PATH + ['supervisor', 'conf.d', '%s.conf' % self.narrow])))
    ctl_path = property(lambda self: os.path.join(*(self.BARE_PATH + ['instances', self.owner, self.name])))
    cfg_path = property(lambda self: os.path.join(*(self.BARE_PATH + ['config', '%s.conf' % self.narrow])))
    
    def rpath(self, *parts):
        return os.path.join(self.ctl_path, *parts)
    
    def exists(self, *args, **kwargs): return Nucleon.local.exists(self.ctl_path, *args, **kwargs)
    def chdir(self, *args, **kwargs):  return Nucleon.local.chdir(self.ctl_path, *args, **kwargs)
    def shell(self, *args, **kwargs):  return Nucleon.local.shell(*args, **kwargs)
    
    @property
    def endpoint(self):
        cnt = self.__dict__
        
        cnt['port'] = self.get_port(self.EP_PORT)
        
        return self.EP_MASK % cnt
    
    @property
    def commandline(self):
        return list(self.daemon_call)
    
    def check(self):
        if not Nucleon.local.exists(self.ctl_path):
            Nucleon.local.shell('mkdir', '-p', self.ctl_path)
        
        Nucleon.local.render('nebula/providers/%s.conf' % self.provider, self.cfg_path,
            backend  = self,
            cmd      = ' '.join(self.commandline),
            get_port = self.get_port,
        **self.__dict__)
        
        Nucleon.local.render('cluster/supervisor/backend.conf', self.job_path,
            backend  = self,
            cmd      = ' '.join(self.commandline),
            get_port = self.get_port,
        **self.__dict__)
        
        if not self.exists('workdir'):
            self.shell('mkdir', '-p', 'workdir')
            
            self.setup()
        
        if callable(getattr(self, 'ensure', None)):
            self.ensure()
    
    def run(self):
        Nucleon.local.shell(*self.commandline)
    
    def debug(self):
        Nucleon.local.shell(*self.commandline)

