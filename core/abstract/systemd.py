#-*- coding: utf-8 -*-

from python2use.core.meta import *

class Daemon(object):
    @classmethod
    def register(cls, hnd):
        if hasattr(hnd, '__name__'):
            svc = hnd()
            
            if svc.name not in Nucleon.daemons:
                Nucleon.daemons[svc.name] = svc
        
        return hnd
    
    def __init__(self):
        super(Daemon, self).__init__()
    
    name     = property(lambda self: self.__class__.__name__.lower())
    
    def control(self, acte):
        for key in getattr(self, 'SERVICEs', [self.name]):
            if acte in ['start', 'restart', 'stop', 'status','reload']:
                if Nucleon.local.exists('/etc/init/%s.conf' % key):
                    Nucleon.local.shell(acte, key)
                elif Nucleon.local.exists('/etc/init.d/%s' % key):
                    Nucleon.local.shell('service', key, acte)
            else:
                pass
    
    def render_tpl(self, tpl, pth, **context):
        Nucleon.local.render('/'.join([self.TPL_ROOT, tpl]), pth, **context)
    
    def render(self, tpl, pth, **context):
        return self.render_tpl('/'.join([self.name,tpl]), pth, **context)
    
    def clean_config(self):
        pass
    
    def config_reflector(self, pb, **cnt):
        pass

class ClusterDaemon(Daemon):
    PORTs = {
        'nginx':      dict(http=80),
        'apache2':    dict(http=8090),
        'bind9':      dict(dns=53),
        'supervisor': dict(http=60301),
    }
    
    def __init__(self):
        super(ClusterDaemon, self).__init__()
        
        if not Nucleon.local.exists(self.CFG_TARGET):
            Nucleon.local.shell('mkdir', '-p', self.CFG_TARGET)
    
    SPARSE     = 'vhost'
    TPL_ROOT   = 'cluster'
    
    ports = property(lambda self: self.PORTs[self.name])
    
    @property
    def context(self):
        return dict(
            name       = self.name,
            ports      = self.ports,
            cfg_target = self.CFG_TARGET,
        )
    
    def clean_config(self):
        for pth in os.listdir(self.CFG_TARGET):
            m = self.CFG_REGEX.match(pth)
            
            if m:
                Nucleon.local.shell('rm', os.path.join(self.CFG_TARGET, pth))
    
    def config_reflector(self, pb, **cnt):
        cnt.update(self.context)
        
        self.render('site.conf', os.path.join(self.CFG_TARGET, self.CFG_LOCATION % pb.site.name),
            cfg    = pb,
            site   = pb.site,
            domain = pb.domain,
            flat_fqdns = ' '.join(pb.http_fqdns),
        **cnt)
    
    def config_vhost(self, vht, **cnt):
        cnt.update(self.context)
        
        src = Nucleon.local.join(self.CFG_TARGET, self.CFG_LOCATION % vht)
        
        #print "\t-> %s ..." % src
        
        self.render('vhost.conf', src,
            daemons = self.PORTs,
            fqdns   = vht.fqdns,
            vhost   = vht,
            dynamix = vht.repo.dynamic_recipe(self),
        **cnt)
    
    def status_vhost(self, vht):
        dst = Nucleon.local.join(self.CFG_DEPLOY, self.CFG_LOCATION % vht)
        
        return Nucleon.local.exists(dst)
    
    def enable_vhost(self, vht):
        src = Nucleon.local.join(self.CFG_TARGET, self.CFG_LOCATION % vht)
        dst = Nucleon.local.join(self.CFG_DEPLOY, self.CFG_LOCATION % vht)
        
        if Nucleon.local.exists(src):
            Nucleon.local.shell('ln', '-sf', src, dst)
    
    def disable_vhost(self, vht):
        dst = Nucleon.local.join(self.CFG_DEPLOY, self.CFG_LOCATION % vht)
        
        Nucleon.local.shell('rm', '-f', dst)

