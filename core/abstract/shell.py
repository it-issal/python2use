#-*- coding: utf-8 -*-

from python2use.core.meta import *

class ShellCommand(Helper):
    @property
    def config(self):
        if not hasattr(self, '_cfg'):
            if hasattr(self, 'KEY'):
                if not Nucleon.local.exists('/hml/etc/%s.yml' % self.KEY):
                    Nucleon.local.shell('cp', '-f', '/shl/etc/defaults/%s.yml' % self.KEY, '/hml/etc/%s.yml' % self.KEY)
                    
                    Nucleon.local.shell('editor', '/hml/etc/%s.yml' % self.KEY)
                
                dtn = yaml.load(open('/hml/etc/%s.yml' % self.KEY).read())
                
                setattr(self, '_cfg', dtn)
        
        return getattr(self, '_cfg', {})
    
    def invoke(self, *args):
        self._prs = argparse.ArgumentParser(description=self.TITLE)
        
        if not hasattr(self, 'FLAGs'):
            self.parser.add_argument('cmd', help='Command to execute.')
            
            self.parser.add_argument('args',   help='Command to execute.', nargs="*", default=[])
        
        if hasattr(self, 'initialize'):
            self.initialize()
        
        self.parser.add_argument('--dry', dest='dryrun', action='store_true',
                            default=False, help="Dry run (not executing anything).")
        
        self.parser.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                            default=False, help="Activated the verbose mode.")
        
        opts = vars(self.parser.parse_args())
        
        if not Nucleon.local.exists('/hml/etc/shelter'):
            Nucleon.local.shell('mkdir', '-p', '/hml/etc/shelter')
        
        if hasattr(self, 'prepare'):
            self.prepare(**opts)
        
        hnd = None
        
        if hasattr(self, 'FLAGs'):
            for flg in self.FLAGs:
                if opts[flg] and hasattr(self, 'do_%s' % flg):
                    hnd = getattr(self, 'do_%s' % flg)
        else:
            if hasattr(self, 'do_%(cmd)s' % opts):
                hnd = getattr(self, 'do_%(cmd)s' % opts)
        
        if callable(hnd):
            setattr(self, 'args', opts.get('args', None) or [])
            
            if 'args' in opts:
                del opts['args']
            
            hnd(*args, **opts)
        elif hasattr(self, 'FLAGs'):
            self.do_native()
        else:
            print "Operation not supported !"
            print
            
            if hasattr(self, 'FLAGs'):
                print "Try one of these commands instead :"
                
                for flg in self.FLAGs:
                    print "\t-> %s" % flg
            
            print
            print prs.print_help()
        
        if hasattr(self, 'terminate'):
            self.terminate(**opts)
        
        delattr(self, 'args')

class Commandline(Helper):
    def __init__(self):
        self._cmds = {}
        
        for key in dir(self):
            hnd = getattr(self, key, None)
            
            if inspect.isclass(hnd):
                if reduce(operator.or_, [issubclass(hnd,cls) for cls in (self.Directive, PersistentDirective)]):
                    obj = hnd(self)
                    
                    self._cmds[obj.name] = obj
        
        if hasattr(self, 'TPL_PATH'):
            self._env = jinja2.Environment(
                loader=jinja2.FileSystemLoader(self.TPL_PATH),
                extensions=[
                    'jinja2.ext.autoescape',
                    'jinja2.ext.do',
                    'jinja2.ext.i18n',
                    'jinja2.ext.loopcontrols',
                    'jinja2.ext.with_',
                ],
            undefined=jinja2.DebugUndefined)
        
        if hasattr(self, 'initialize'):
            self.initialize()
    
    #************************************************************************************
    
    def __getitem__(self, key):
        return self.__dict__[key]
    
    def __call__(self, prg, dct, *args):
        if hasattr(self, 'prepare'):
            self.prepare(*args)
        
        cmd = None
        
        if dct in self._cmds:
            cmd = self._cmds[dct]
            
            self._curr = cmd
        
        if callable(cmd):
            cmd(*args)
        elif dct=='help':
            if len(args)==0:
                print "Thanks for your enthusiasm ! Try one of this commands :"
                
                for cmd in self._cmds.values():
                    print "\t*) %s \t : %s" % (cmd.name, cmd.help_msg)
            else:
                if args[0] in self._cmds:
                    print self._cmds[args[0]].print_help()
        else:
            print "Operation not supported ! Try one of this commands :"
            
            for cmd in self._cmds.values():
                    print "\t*) %s \t : %s" % (cmd.name, cmd.help_msg)
        
        if hasattr(self, 'terminate'):
            self.terminate(*args)
    
    #************************************************************************************
    
    def render(self, template_path, target_path, **context):
        context.update(self.__dict__)
        
        if hasattr(self, '_env'):
            try:
                tpl = self._env.get_template(template_path)
            except jinja2.TemplateNotFound,ex:
                print "Template not found : %s" % template_path
                
                raise ex
                
                return ""
            
            resp = tpl.render(**context)
            
            return Nucleon.local.write(target_path, resp)
        else:
            return Nucleon.local.render(template_path, target_path, **context)
    
    #************************************************************************************
    
    class Directive(object):
        def __init__(self, parent):
            self._prn = parent
            self._prs = argparse.ArgumentParser(description=getattr(self, 'HELP', None))
            
            if hasattr(self, 'initialize'):
                if callable(self.initialize):
                    self.initialize()
        
        name     = property(lambda self: self.__class__.__name__.lower())
        parser   = property(lambda self: self._prs)
        help_msg = property(lambda self: self.parser.description)
        
        parent   = property(lambda self: self._prn)
        config   = property(lambda self: self.parent.config)
        
        #************************************************************************************
        
        def add_arg(self, key, cardinality='*', default=[], help=None):
            return self.parser.add_argument(key, nargs=cardinality, default=False, help=help or "")
        
        def add_flag(self, key, *slugs, **kwargs):
            for k,v in dict(dest=key, action='store_true', default=False, help="").iteritems():
                if (k not in kwargs):
                    kwargs[k] = v
            
            for k,v in dict(help="").iteritems():
                kwargs[k] = kwargs[k] or v
            
            return self.parser.add_argument(*slugs, **kwargs)
        
        def add_param(self, key, short_slug, long_slug, default=None, help=None):
            return self.parser.add_argument(short_slug, long_slug, dest=key, type=str, default=default or "", help=help or "")
        
        #************************************************************************************
        
        def render(self, *args, **kwargs):
            kwargs.update(self.__dict__)
            
            return Nucleon.local.render(*args, **kwargs)
        
        def __call__(self, *args):
            kargs = self.parser.parse_args(args)
            
            nargs = set([]) ; vargs = vars(kargs)
            
            if 'args' in vargs:
                nargs = vargs['args'] ; del vargs['args']
            
            if hasattr(self.parent, 'before_command'):
                self.parent.before_command(*nargs, **vargs)
            
            resp = self.execute(*nargs, **vargs)
            
            if hasattr(self.parent, 'after_command'):
                self.parent.after_command(*nargs, **vargs)
            
            return resp
        
        #************************************************************************************
        
        @property
        def __dict__(self):
            resp = self.parent.__dict__
            
            resp.update(self.args)
            
            return resp
        
        def __getitem__(self, key):
            return self.__dict__[key]
    
    #************************************************************************************
    
    @property
    def __dict__(self):
        resp = self.context
        
        #resp.update(self.args)
        
        return resp

#######################################################################################################################################

class PersistentCommandline(Commandline):
    def initialize(self):
        if not Nucleon.local.exists('/hml/etc/shelter'):
            Nucleon.local.shell('mkdir', '-p', '/hml/etc/shelter')
    
    #************************************************************************************
    
    @property
    def config(self):
        if not hasattr(self, '_cfg'):
            if hasattr(self, 'KEY'):
                if not Nucleon.local.exists('/hml/etc/%s.yml' % self.KEY):
                    Nucleon.local.shell('cp', '-f', '/shl/etc/defaults/%s.yml' % self.KEY, '/hml/etc/%s.yml' % self.KEY)
                    
                    Nucleon.local.shell('editor', '/hml/etc/%s.yml' % self.KEY)
                
                dtn = yaml.load(open('/hml/etc/%s.yml' % self.KEY).read())
                
                setattr(self, '_cfg', dtn)
        
        return getattr(self, '_cfg', {})
    
    #************************************************************************************
    
    def prepare(self, *args):
        if not Nucleon.local.exists('/hml/etc/shelter'):
            Nucleon.local.shell('mkdir', '-p', '/hml/etc/shelter')
        
        self.cfg = Nucleon.local.read_yaml(self.CFG_PATH)
        
        if hasattr(self, 'load_config'):
            self.load_config(*args)
    
    def terminate(self, *args):
        if hasattr(self, 'pre_save'):
            self.pre_save(*args)
        
        Nucleon.local.write_yaml(self.CFG_PATH, self.cfg)
        
        if hasattr(self, 'post_save'):
            self.post_save(*args)

#######################################################################################################################################

class PersistentDirective(Commandline.Directive):
    def initialize(self, *args, **kwargs):
        if hasattr(self, 'configure'):
            if callable(self.configure):
                self.configure()
        
        if hasattr(self.parent, 'default_args'):
            if callable(self.parent.default_args):
                self.parent.default_args(self.parser)
        
        self.add_flag('dry',           '--dry',     help="Dry Run : (Don't execute anything, neither save to configuration file).")
        self.add_flag('verbose', '-v', '--verbose', help="Augment the verbose mode.")

