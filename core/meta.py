#-*- coding: utf-8 -*-

from python2use.core.utils import *

class Helper(object):
    pass

class Manageable(Helper):
    def __init__(self, mgr, bpath, owner, name, **kwargs):
        super(Manageable, self).__init__()
        
        self._mgr   = mgr
        self._bpath = bpath
        self._owner = owner
        self._name  = name
        
        for k,v,d in [
            ('_cfg',   'config', {}),
            ('_ports', 'ports', {}),
        ]:
            setattr(self, k, kwargs.get(v, d))
            
            if v in kwargs:
                del kwargs[v]
        
        self._args  = kwargs
    
    manager  = property(lambda self: self._mgr)
    owner    = property(lambda self: self._owner)
    name     = property(lambda self: self._name)
    
    narrow   = property(lambda self: self.TPL_NARROW % dict(owner=self.owner, name=self.name))
    
    #bpath    = property(lambda self: Nucleon.local.join(self._bpath, 'srv', self.SUBDIR_NARROW, self.owner, self.name))
    bpath    = property(lambda self: self._bpath)
    
    config   = property(lambda self: self._cfg)
    ports    = property(lambda self: self._ports)
    
    extra    = property(lambda self: self._args)
    
    def get_port(self, key):
        if key not in self._ports:
            self._ports[key] = random.randrange(50000, 50999, 1)
        
        return self._ports[key]
    
    @property
    def __soul__(self):
        resp = {}
        
        for key in ('owner','name')+self.CONTEXT_CORE:
            resp[key] = getattr(self, key, None)
        
        if hasattr(self, 'parameters'):
            if callable(self.parameters):
                self.parameters(resp)
        
        return resp
    
    @property
    def __dict__(self):
        resp = self.__soul__
        
        for key in ('narrow',)+self.CONTEXT_EXTRA:
            resp[key] = getattr(self, key, None)
        
        if hasattr(self, 'parameters'):
            if callable(self.parameters):
                self.parameters(resp)
        
        return resp
    
    def __getitem__(self, *args, **kwargs):
        return self.__dict__.get(*args, **kwargs)

