#-*- coding: utf-8 -*-

from python2use.core.libs import *

class BaseSystem(object):
    def __init__(self, mgr):
        self._mgr = mgr
        
        self._dmn = {}
        
        from python2use.core.utils import Nucleon
        
        for k in Nucleon.daemons:
            hnd = Nucleon.daemons[k]
            
            self._dmn[k] = hnd
    
    manager = property(lambda self: self._mgr)
    daemons = property(lambda self: self._dmn)
    
    #************************************************************************************
    
    def read_yaml(self, pth):
        return yaml.load(self.read(pth))
    
    def write_yaml(self, pth, data):
        self.write(pth, yaml.dump(data, default_flow_style=False, indent=2))
    
    #************************************************************************************
    
    def read_json(self, pth):
        return json.loads(self.read(pth))
    
    def write_json(self, pth, data):
        self.write(pth, json.dumps(data))
    
    #************************************************************************************
    
    def mkdir(self, *paths):
        self.shell('mkdir', '-p', *paths)

#########################################################################################

class LocalSystem(BaseSystem):
    TEMPLATES_ROOT = '/shl/usr/share/nebula/config'
    
    DRY_RUN = False
    
    def __init__(self, *args, **kwargs):
        super(LocalSystem, self).__init__(*args, **kwargs)
        
        self._env = jinja2.Environment(
            loader=jinja2.FileSystemLoader(self.TEMPLATES_ROOT),
            extensions=[
                'jinja2.ext.autoescape',
                'jinja2.ext.do',
                'jinja2.ext.i18n',
                'jinja2.ext.loopcontrols',
                'jinja2.ext.with_',
            ],
        undefined=jinja2.DebugUndefined)
    
    hostname = property(lambda self: socket.gethostname())
    hostfqdn = property(lambda self: socket.gethostname())
    username = property(lambda self: os.environ['USER'])
    homedir  = property(lambda self: os.environ['HOME'])
    currdir  = property(lambda self: os.environ['PWD'])
    
    #************************************************************************************
    
    def read(self, pth, split=None):
        resp = open(pth).read()
        
        if split:
            resp = resp.split(split)
        
        return resp
    
    def write(self, pth, *lns):
        src = '\n'.join(lns)
        
        f = open(pth, 'w+')
        
        f.write(src.encode('utf-8'))
        
        f.close()
        
        return src
    
    #************************************************************************************
    
    def read_tpl(self, pth, *args, **kwargs):
        rpath = self.join(self.TEMPLATES_ROOT, pth)
        
        return self.read(rpath, *args, **kwargs)
    
    def render(self, template_path, target_path, **context):
        try:
            tpl = self._env.get_template(template_path)
        except jinja2.TemplateNotFound,ex:
            print "Template not found : %s" % template_path
            
            raise ex
            
            return ""
        
        resp = tpl.render(**context)
        
        return self.write(target_path, resp)
    
    #************************************************************************************
    
    def join(self, *args, **kwargs):
        return os.path.join(*args)
    
    def exists(self, *args, **kwargs):
        return os.path.exists(self.join(*args), **kwargs)
    
    def lsdir(self, *args, **kwargs):
        return os.listdir(self.join(*args), **kwargs)
    
    def chdir(self, *args, **kwargs):
        return os.chdir(self.join(*args), **kwargs)
    
    #************************************************************************************
    
    def shell(self, *cmd):
        stmt = []
        
        for s in cmd:
            if ' ' in s:
                stmt.append('"'+s+'"')
            else:
                stmt.append(s)
        
        stmt = ' '.join(stmt)
        
        if self.DRY_RUN:
            print stmt
        else:
            os.system(stmt)
    
    def process(self, prg, *params, **opts):
        #p = subprocess.Popen(prg, args, shell=True, bufsize=bufsize, close_fds=True)
        p = subprocess.Popen([prg]+list(params), stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        
        if opts.get('wait', False):
            while p.pid:
                eventlet.sleep()
            
            #p.wait()
        
        return p
    
    def execute(self, prg, *params, **opts):
        return self.process(prg, *params, **opts).stdout.read()
    
    #************************************************************************************
    
    def xterm(self, *args):
        cmd = ' '.join(args)
        
        return Nucleon.local.shell('gnome-terminal', '-t', self.hostname, '-e', '"'+cmd+'"')

class RemoteSystem(BaseSystem):
    def __init__(self, mgr, host):
        super(RemoteSystem, self).__init__(mgr)
        
        self._target = host
        
        self._conn = None
    
    hostname = property(lambda self: self._target)
    
    #************************************************************************************
    
    @property
    def cnx(self):
        if not hasattr(self, '_conn'):
            self._conn = None
        
        if self._conn is None:
            key = paramiko.RSAKey(data=base64.decodestring('AAA...'))
            
            self._conn = paramiko.SSHClient()
            
            self._conn.get_host_keys().add(self.hostname, 'ssh-rsa', key)
            
            self._conn.connect(self.hostname, username='strongbad', password='thecheat')
            
            #self._conn.close()
        
        return self._conn
    
    #************************************************************************************
    
    def execute(self, *cmd):
        stdin, stdout, stderr = self.cnx.exec_command(*cmd)
        
        for line in stdout:
            print '... ' + line.strip('\n')

