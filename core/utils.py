#-*- coding: utf-8 -*-

from .helpers import *
from .wrappers.hosts import *

class Nucleon(object):
    SPARKLE_ON   = '-pur.pl'
    
    def __init__(self):
        self._cache = redis.StrictRedis(host='srv-cache', port=6379, db=0)
        
        try:
            self._queue = eventlet.GreenPool()
        except:
            self._queue = None
        
        self._reg = {
            'backend': {},
            'daemon':  {},
            'feature': {},
            'flavor':  [],
            'remote':  {},
            'vhost':   {},
        }
    
    ##############################################################################
    
    cache    = property(lambda self: self._cache)
    
    backends = property(lambda self: self._reg['backend'])
    daemons  = property(lambda self: self._reg['daemon'])
    features = property(lambda self: self._reg['feature'])
    flavors  = property(lambda self: self._reg['flavor'])
    vhosts   = property(lambda self: self._reg['vhost'])
    
    def get_vhost(self, mgr, bpath, **narrow):
        from voodoo.shortcuts import vHost 
        
        nrw = vHost.TPL_NARROW % narrow
        
        resp = None
        
        if nrw in self._reg['vhost']:
            resp = self._reg['vhost'][nrw]
        else:
            resp = vHost(mgr, bpath, **narrow)
            
            if resp:
                self._reg['vhost'][resp.narrow] = resp
        
        return resp
    
    def get_backend(self, narrow, **kwargs):
        resp = None
        
        if narrow in self._reg['backend']:
            return self._reg['backend'][narrow]
        else:
            if 'provider' not in kwargs:
                print kwargs
            
            if kwargs['provider'] in self.daemons:
                cls = self.daemons[kwargs['provider']]
                
                if hasattr(cls, 'Instance'):
                    resp = cls.Instance(self, **kwargs)
            else:
                print kwargs
                print self.daemons.keys()
            
            if resp:
                self._reg['backend'][resp.narrow] = resp
        
        return resp
    
    ##############################################################################
    
    local    = property(lambda self: self.remote('localhost'))
    
    def remote(self, host):
        if host not in self._reg['remote']:
            obj = None
            
            if host in ['localhost']:
                obj = LocalSystem(self)
            else:
                obj = RemoteSystem(self, host)
            
            if obj:
                self._reg['remote'][host] = obj
        
        return self._reg['remote'].get(host, None)
    
    ##############################################################################
    
    def sleep(self, *args, **kwargs):
        return eventlet.sleep(*args, **kwargs)
    
    def thread(self, *args, **kwargs):
        mq = eventlet.GreenPile(self._queue)
        
        return mq.spawn(self.local.process, *args, **kwargs)
    
    def loop(self):
        try:
            self._queue.waitall()
        except (KeyboardInterrupt, SystemExit):
            pass

Nucleon = Nucleon()

