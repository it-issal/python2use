#-*- coding: utf-8 -*-

from .libs import *

from uuid import uuid4

from hurry.filesize import size as human_size

def timestamp(dt):
    return calendar.timegm(dt.utctimetuple())

def iif(condition, true_value=True, false_value=False):
    if condition:
        return true_value
    else:
        return false_value

def singleton(handler):
    return handler()

def reflect(*args, **kwargs):
    def do_apply(handler, callback):
        callback(handler)
        
        return handler
    
    return lambda hnd: do_apply(hnd, *args, **kwargs)

SSH_SERVICEs = {
    'mysql':   dict(port=[3306],  cmd='mysql'),
    'amqp':    dict(port=[5672],  cmd='amqp'),
    'couchdb': dict(port=[5984],  cmd='couch'),
    'redis':   dict(port=[6379],  cmd='redis'),
    'elastic': dict(port=[9200],  cmd='es'),
    'mongodb': dict(port=[27000], cmd='mongo'),
    
    'maher':   dict(port=[9000+i for i in range(1, 3)], cmd='maher'),
    'nebula':  dict(port=[9010+i for i in range(1, 7)], cmd='nebula'),
    'servlet': dict(port=[9030+i for i in range(1, 7)], cmd='servlet'),
}

def ssh_command(host, user=os.environ.get('USER', 'root'), port=22, x11=False, **kwargs):
    cmd = ['ssh']
    
    if x11:
        cmd.append('-X')
    
    for svc in SSH_SERVICEs.values():
        if kwargs.get('tunnel_%(name)s' % svc, False) or kwargs['full']:
            for prt in svc['port']:
                cmd.append('-L')
                cmd.append("%d:%s:%d" % (prt, svc['remote'], prt))
    
    cmd.append('%s@%s -p %d' % (user, host, port))
    
    return cmd

