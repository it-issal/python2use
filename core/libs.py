#-*- coding: utf-8 -*-

from __future__ import absolute_import

from new import instancemethod

import os, sys, inspect, operator, argparse, subprocess, socket, re, uuid, hashlib, random

import yaml, simplejson as json

from optparse import make_option
from uuid import uuid4

from datetime import time, date, datetime, timedelta
import dateutil.parser
import calendar

from urlparse import urlparse, parse_qs, urljoin
import urllib2

import jinja2, requests
import eventlet, rdflib, git
import redis

